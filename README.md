Instructions for developing a Flux UI Widget.
========================================

This doc describes how to develop a UI widget for use in the Flux Maker Tools.

## How Custom Widgets work

To add a custom widget to the UI of your Flux project, you can add the Custom Component element to your canvas. This element embeds an iframe, the content of which will be your custom widget.

To develop a widget locally and see it embedded in the Flux UI, you'll need to run a web server on your local machine. If you have python installed, you can run

```bash
python simple_https_server.py
```

You'll notice (in the Inspector) that Custom Component has three special properties: port, localPath, and token. When developing locally, set port to the port on which you're serving and localPath (optional) to the relative URL of the html file you want to display.

When you first interact with a custom component that you are developing locally, you will need to navigate in another tab to the url displayed at the bottom of the custom component display to accept the self-signed certificate, then refresh the UI composer.

## Interacting with Flux Flow

All UI elements interact with the flow backend by reading and writing flow values. Your element will need to do this as well.

If you're using [Polymer](https://www.polymer-project.org/) (recommended), you can use the provided [Behavior](https://www.polymer-project.org/1.0/docs/devguide/behaviors.html): Flux.externalEltMixin. See elements/rgb-widget.html for an example of how to do this.

The flux mixin will expose all the [declared properties](https://www.polymer-project.org/1.0/docs/devguide/properties.html) of your polymer element as bindable attributes in Inspector panel, and you can bind them the same way you bind attributes of any Flux UI widget.

NOTE: To trigger a flow update when a property of an Object or Array value changes, you'll need to do a wholesale replacement of the Object/Array. Flow updates will also come over the wire as wholesale replacements.

## Flux Flow API

If you aren't using Polymer, that's OK -- you'll just need to implement communication with the parent window yourself.

Your widget will communicate with the parent window via [window.postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage). The parent element (that Custom Component element you added to your UI) expects one message of the following form on load:

```json
{
    type: 'properties',
    properties: ['foo', 'bar', 'baz']
}
```

Then, for every flow value change, you should send and receive messages of this form:

```json
{
    type: 'update',
    key: 'foo',
    value: 'new value of foo',
    tick: TICK_VALUE
}
```

where your message echos back the most recent TICK_VALUE received for this flow key.

## Sharing your UI Widget.

Once you've developed a custom widget, you'll probably want to make it available to others (your collaborators and/or users of your App). To do that, you'll replace the port/localPath you've been using for development with a token (in the token field of the Inspector).

To get a token, you'll need to bundle your component and any dependencies together into a single HTML file and (for now) email that file to drew@flux.io. I'll upload it and give you back a token.

If you're using polymer (or are otherwise using HTML Imports to load dependencies), you can use polymer's [vulcanize](https://github.com/polymer/vulcanize) to do this. Install it:
```bash
npm install -g vulcanize
```
and then vulcanize your file, inlining all scripts and css:
```bash
vulcanize --inline path/to/file
```

