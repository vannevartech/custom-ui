import BaseHTTPServer, SimpleHTTPServer
import ssl

class CORSRequestHandler (SimpleHTTPServer.SimpleHTTPRequestHandler):
    def end_headers (self):
        self.send_header('Access-Control-Allow-Origin', '*')
        SimpleHTTPServer.SimpleHTTPRequestHandler.end_headers(self)


print "Starting server... https://localhost:4443"

httpd = BaseHTTPServer.HTTPServer(('localhost', 4443), CORSRequestHandler)
httpd.socket = ssl.wrap_socket(httpd.socket, certfile='./cert.pem', server_side=True)

httpd.serve_forever()