Flux = {
    externalEltBehavior: {

        getAllPublishedProperties: function() {
            function getPublishedRecursive(elt) {
                if (elt === null) { return []; }
                return Object.keys(elt)
                .concat(getPublishedRecursive(Object.getPrototypeOf(elt)));
            };
            return getPublishedRecursive(this.properties);
        },

        ready: function() {
            window.addEventListener('message', function(event) {
                if (event.data.type==='update') {
                    this[event.data.key] = event.data.value;
                    this.lastUpdated_[event.data.key] = event.data.tick;
                }
            }.bind(this), false);
            var msg = {
                type: 'properties',
                properties: this.getAllPublishedProperties()
            };
            parent.postMessage(msg, '*', window);

            this.propertyObservers_ = {};
            this.lastUpdated_ = {};
            this.getAllPublishedProperties().forEach(function (propertyName) {
                //TODO(Drew): why doesn't (this, propertyName) work?
                var observer = new PathObserver(this, ['__data__', propertyName]);
                observer.open(function(newValue, oldValue) {
                    var msg = {
                        type: 'update',
                        key: propertyName,
                        value: newValue,
                        tick: this.lastUpdated_[propertyName]
                    };
                    parent.postMessage(msg, '*', window);
                }.bind(this));
                this.propertyObservers_[propertyName] = observer;
            }, this);
        },
    }
};